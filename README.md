# gitlab-runner

# Running:
First, you must configure the _`config/config.toml`_ with your runner url & token. After doing that, you can easely run the image with:

```
$ docker-compose up -d
```

To stop the container, use:
```
$ docker-compose down
```

## Interactive configuration:
If you wish to configure your image interactively, you can run the followinf steps:

```
$ docker-compose up -d
$ docker-compose exec -T gitlab-runner gitlab-runner register
```
